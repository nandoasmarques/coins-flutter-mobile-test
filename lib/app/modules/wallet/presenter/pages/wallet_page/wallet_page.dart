import 'package:coins_test/app/modules/wallet/presenter/pages/wallet_page/states/wallet_error.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controllers/wallet_controller.dart';
import 'states/wallet_loading.dart';
import 'states/wallet_success.dart';

class WalletPage extends StatelessWidget {
  const WalletPage(this._controller, {Key? key}) : super(key: key);

  final WalletController _controller;

  @override
  Widget build(BuildContext context) {
    return _controller.obx(
        (state) {
          return WalletSuccess(state);
        },
        onLoading: const WalletLoading(),
        onError: (errorMessage) {
          return WalletError(exceptionMessage: errorMessage ?? '');
        });
  }
}
